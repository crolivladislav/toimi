import './styles/style.scss'
import './styles/header.scss'
import './styles/footer.scss'
import './styles/contacts.scss'
import './styles/forms.scss'

window.onload = function () {
	let airButton = document.querySelector('.air__button');
	let airText = document.querySelectorAll('.air__text');

	airButton.addEventListener("click", function (e) {
		airButton.classList.add('sent');
		for (let i of airText) {
			i.classList.toggle('hidden');
		}
		setTimeout(() => {
			airButton.classList.remove('sent');
			for (let i of airText) {
				i.classList.toggle('hidden');
			}
			document.activeElement.blur();
		}, "2000")
	})
}